# Source code for The WebConf submission number 1965

## Data Sets

First, run `unzip datasets.zip`.

The folder `datasets` then contains the used data sets. Please refer to the paper for references of the datat sets.

The data set format is the following the number of nodes in the first line
followed by the chronologically ordered sequence of temporal edges 
`u v c` with u and v being nodes, and c the color of the edge.

For example:
```
0 1 1
1 2 0
2 0 1
```
is a graph with three nodes and three edges.
The data sets included in the repository are already in this format.



## Compilation of the C++ code
The algorithms are implemented in C++. 
To compile the executable, create a folder `release` in `source/ecdsp`. 
Change into this folder and run

    cmake -DCMAKE_BUILD_TYPE=Release ..

Finally, run `make` to compile the code and get the executable file `ecdsp`.


## General Usage

Just running `./ecdsp` in `release` will provide basic information about parameters. 

Example:

* `./ecdsp ../../../datasets/aucs.txt -t=8` gives information about the AUCS data set
* `./ecdsp ../../../datasets/aucs.txt -t=0 -x=1000` approximates the at least 1000 edges DSP
* `./ecdsp ../../../datasets/aucs.txt -t=1 -x=100` approximates the at least h edges DSP for increasing values of h
* `./ecdsp ../../../datasets/aucs.txt -t=3 -c=0 10 -c=2 5` approximates the at least h colred edges DSP with h_0=10 and h_2=5

 
## Running the Experiments

For running the ILPs you need Gurobi 9 setup for using with Python.
Run the `ecdsp` steps for all data sets.

### Q1.

* First, run ` ./ecdsp ../../../datasets/aucs.txt -t=1 -x=1` to obtain the approximation results for the at least h edges DSP.
* Next, run `ilp_atleasthedges.py` in the folder `python_code/ilp/` to compute the exact results using the ILP.
* Finally, run `exact_comparison.py` in the folder `python_code/visualivation` to obtain the error statistics.

### Q2.

* Run `./ecdsp ../../../datasets/aucs.txt -t=1 -x=100` and `./ecdsp ../../../datasets/aucs.txt -t=2 -x=10`
* Next, run `atleasthedgesdsp_from_file.py` and `atleasthedgesdsp_rt_from_file` in the folder `python_code/visualivation` 

### Q3.

* Run `./ecdsp ../../../datasets/aucs.txt -t=8` to obtain the color distributions
* Compute the exact unconstrained DSP and obtain the color distribution
* Run run `color_distribution.py` in the folder `python_code/visualivation`

### Q4.

* First, run ` ./ecdsp ../../../datasets/aucs.txt -t=4 -x=100` to obtain the random instances and approximation results for the at least h colored edges DSP.
* Next, run `ilp_atleasthcoloredges.py` in the folder `python_code/ilp/` to compute the exact results using the ILP.
* Finally, run `exact_comparison_colors.py` in the folder `python_code/visualivation` to obtain the error statistics.

### Q5.

* Run ` ./ecdsp ../../../datasets/aucs.txt -t=5 -x=10` to obtain the random instances and approximation results for the at least h colored edges DSP.
* Run ` ./ecdsp ../../../datasets/aucs.txt -t=5 -x=10 -m=1` to obtain the random instances and approximation results for the at least h colored edges DSP.
* Finally, run `color_fractions_withm.py` in the folder `python_code/visualivation`.

### Q6.

* Run ` ./ecdsp ../../../datasets/aucs.txt -t=6 -x=10` to obtain the random instances and approximation results for the at least h colored edges DSP.
* Finally, run `color_runningtimes.py` in the folder `python_code/visualivation`.


### Use case

First run

* `./ecdsp ../../../datasets/dblp_usecase.txt -t=3 -x=10`
* `./ecdsp ../../../datasets/dblp_usecase.txt -t=3 -x=100`
* `./ecdsp ../../../datasets/dblp_usecase.txt -t=3 -x=1000`

Finally, run `color_distc_usecase.py` in the folder `python_code/usecase`.

