import pandas as pd

from utils import readFile, dataset_path
import seaborn as sns
from matplotlib import pyplot as plt

def parse_file(filename):
    data = readFile(filename)
    densities1 = []
    times1 = []
    densities2 = []
    times2 = []
    densities3 = []
    times3 = []
    for line in data:
        s = line.split(',')
        d1 = s[1].split(" ")
        densities1.append(float(d1[0]))
        times1.append(float(d1[1]))
        d2 = s[3].split(" ")
        densities2.append(float(d2[0]))
        times2.append(float(d2[1]))
        d3 = s[5].split(" ")
        densities3.append(float(d3[0]))
        times3.append(float(d3[1]))

    print(densities1)
    print(densities2)
    print(densities3)
    print(times1)
    print(times2)
    print(times3)

    return densities1, times1, densities3, times3


def read_color_fractions_file(dataset):
    densities1, times1, densities3, times3 = parse_file(dataset_path + dataset + ".txt_colorfractions_m0.txt")
    mdensities1, mtimes1, mdensities3, mtimes3 = parse_file(dataset_path + dataset + ".txt_colorfractions_m1.txt")


    x = list(range(1,len(densities1)+1)) + list(range(1,len(densities3)+1)) + list(range(1,len(mdensities3)+1))
    print(len(x), len(densities1+densities3+mdensities3))
    alg = ["ColApprox"] * len(densities1) + ["Heuristic"] * len(densities3) + ["Heuristic$^*$"] * len(mdensities3)
    d = {
        "x" : x,
        "y" : densities1 + densities3  + mdensities3 ,
        "t" : times1 + times3 + mtimes3 ,
        "Alg." : alg
    }
    df = pd.DataFrame(d)
    sns.set(rc={'figure.figsize': (4, 3)})
    sns.set_style("whitegrid")
    ax = sns.lineplot(data=df, x="x", y="y", hue="Alg.", style='Alg.', linewidth=2, dashes=True, alpha=0.9)
    ax.set(xlabel='$i$', ylabel="Density")
    plt.gca().get_legend().set_title("")
    plt.tight_layout()
    plt.savefig(dataset + '_colorfraction.pdf')
    plt.show()



if __name__ == '__main__':
    datasets = ["aucs", "airports", "rattus", "knowledge", "epinions", "twitter",
                "hospital", "htmlconf", "fftwyt", "friendfeed", "homo", "dblp"]

    for ds in datasets:
        read_color_fractions_file(ds)


