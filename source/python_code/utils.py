dataset_path = "../../../datasets/"


def readFile(filename):
    with open(filename) as f:
        content = f.readlines()
    data = [x.strip() for x in content]
    return data
