#include <sstream>
#include <iostream>
#include <fstream>
#include "graph.h"
#include "../util/IO.h"

using namespace std;


graph load_graph(std::string const &file, bool no_cols) {
    std::ifstream fs;
    fs.open(file);
    auto opened_file = fs.is_open();

    if (!opened_file) {
        throw std::runtime_error("Could not open file " + file);
    }

    std::string line;
    graph g;

    Color max_color = 0;

    set<edge> edges_set;

    while (getline(fs, line)) {

        if (line.empty()) {
            continue;
        }
        auto l = split_string<NodeId>(line, ' ');

        if (l[0] == l[1]) {
            continue;
        }

        Color c = 0;
        if (l.size() > 2) {
            c = l[2];
        }
        if (no_cols) c = 0;
        edges_set.insert({l[0], l[1], c});
        if (l[1] > g.max_nid) g.max_nid = l[1];
        if (c > max_color) max_color = c;
    }
    fs.close();

    for (auto &e : edges_set) {
        g.edges.push_back(e);
    }

    g.adjacent.resize(g.max_nid + 1, vector<edge>());
    g.neighbors.resize(g.max_nid + 1, set<NodeId>());
    for (auto e : g.edges) {
        g.adjacent[e.u].push_back(e);
        g.adjacent[e.v].push_back(e);
        g.neighbors[e.u].insert(e.v);
        g.neighbors[e.v].insert(e.u);
        g.colors[e.c]++;
    }

    cout << "Loades graph from file " << file << endl;
    cout << "nodes: " << g.max_nid+1 << endl;
    cout << "edges: " << g.edges.size() << endl;

    if (!no_cols) {
        cout << "Edges per color: " << endl;
        vector<double> fractions;
        for (auto &p: g.colors) {
            auto f = (double) p.second / (double) (g.edges.size());
            cout << p.first << "\t" << p.second << "\t" << f << endl;
            fractions.push_back(f);
        }
        for (auto &d: fractions)
            cout << d << ", ";
        cout << endl;
    }
    return g;
}