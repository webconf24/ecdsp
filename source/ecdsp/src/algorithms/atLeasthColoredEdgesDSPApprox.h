#ifndef FAIREDSP_ATLEASTHCOLOREDEDGESDSPAPPROX_H
#define FAIREDSP_ATLEASTHCOLOREDEDGESDSPAPPROX_H


#include "../graph/graph.h"

struct Result {
    double density;
    double rt;
    size_t nodes;
    size_t edges;
    std::map<Color, uint> colors;
    std::set<NodeId> nids{};
};

struct AtLeasthEdgesResult {
    std::pair<std::set<NodeId>, std::vector<edge>> subgraph;
    std::pair<std::set<NodeId>, std::vector<edge>> non_subgraph;
    std::map<Color, uint> edges_of_color_in_subgraph;
    double rt{};
};


Result computeAtLeasthColoredEdgesDSPApproximation(graph const &graph,
                                                   const std::map<Color, uint>& min_color_requirements,
                                                   uint total_min_edges);

#endif //FAIREDSP_ATLEASTHCOLOREDEDGESDSPAPPROX_H
