#ifndef FAIREDSP_UNCONSTRAINEDDSPAPPROX_H
#define FAIREDSP_UNCONSTRAINEDDSPAPPROX_H


#include "../graph/graph.h"

std::pair<uint, uint> computeUnconstrainedDSPApproximation(graph const &g);

std::pair<std::set<NodeId>, std::vector<edge>> computeUnconstrainedDSPApproximationAndGetGraph(graph const &graph);


#endif //FAIREDSP_UNCONSTRAINEDDSPAPPROX_H
