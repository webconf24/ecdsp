#include <iostream>
#include "baseline.h"
#include "atLeasthColoredEdgesDSPApprox.h"
#include "../util/IO.h"

using namespace std;


AtLeasthEdgesResult postprocess_org(/*set<edge> const &edge_set,*/ graph const &graph, const std::map<Color, uint>& min_color_requirements) {
    Timer timer;
    timer.start();

    std::map<uint, std::vector<edge>> g;

    uint edges = graph.colors.size();
    std::map<Color, uint> colors_left = graph.colors;
    set<NodeId> nids;
    NodeId mxnid = 0;


    for (auto &e : graph.edges) {
        g[e.u].push_back(e); // todo assumption graph is undirected
        g[e.v].push_back({e.v, e.u, e.c}); // todo assumption graph is undirected
        nids.insert(e.u);
        nids.insert(e.v);

        if (e.u > mxnid) mxnid = e.u;
        if (e.v > mxnid) mxnid = e.v;
    }

    std::set<std::pair<uint, NodeId>, std::less<>> degrees;
    std::vector<uint> c(mxnid + 1, 0);
    for (auto &p : g) {
        degrees.insert({p.second.size(), p.first});
        c[p.first] = p.second.size();
    }

    std::vector<bool> removed(mxnid+1, false);

    uint nodes = nids.size();
    double max_density = 0;

    std::vector<bool> best_removed(mxnid+1, false);

    auto density = (double)edges/(double)nodes;
    if (density > max_density) {
        max_density = density;
    }

    while (!degrees.empty()) {

        auto next = degrees.begin();
        auto u = next->second;
        degrees.erase(next);

        if (removed[u]) continue;
        removed[u] = true;


        nodes--;

        bool color_violation = false;

        for (auto &e : g[u]) {
            if (removed[e.v]) continue;
            if (c[e.v] > c[e.u]) {
                --c[e.v];
                degrees.insert({c[e.v], e.v});
            }
            if (colors_left[e.c] > 0) {
                colors_left[e.c]--;
            }
            if (min_color_requirements.contains(e.c) && colors_left[e.c] < min_color_requirements.at(e.c)) {
                color_violation = true;
                break;
            }
            edges--;
        }


        if (!color_violation) {
            density = (double) edges / (double) nodes;
            if (density > max_density) {
                max_density = density;
                best_removed = removed;
            }
        } else {
            break;
        }
    }

    set<NodeId> remaining_nids;
    for (NodeId i : nids) {
        if (!best_removed[i])
            remaining_nids.insert(i);
    }


    auto rt = timer.stop();

    AtLeasthEdgesResult result;
    result.rt = rt;

    vector<edge> remaining_edges;
    vector<edge> removed_edges;
    for (auto &e : graph.edges) {
        if (remaining_nids.contains(e.u) && remaining_nids.contains(e.v)) {
            remaining_edges.push_back(e);
            result.edges_of_color_in_subgraph[e.c]++;
        } else {
            removed_edges.push_back(e);
        }
    }

    result.subgraph = {remaining_nids, remaining_edges};
    result.non_subgraph = {{best_removed.begin(), best_removed.end()}, removed_edges};

    return result;
}


Result computeAtLeasthColoredEdgesDSPBaselineHeuristic(graph const &graph,
                                                       const std::map<Color, uint>& min_color_requirements) {

    auto pp_result = postprocess_org(graph, min_color_requirements);

    auto edges = pp_result.subgraph.second;
    auto nodes = pp_result.subgraph.first;

    auto density = (double)edges.size() / (double)nodes.size();

    cout << "----Solution of at least h colored edges BASELINE heuristic----" << endl;

    cout << "Elapsed: " << pp_result.rt << endl;
    cout << "nodes: " << nodes.size() << endl;
    cout << "edges: " << edges.size() << endl;
    cout << "density: " << density << endl;
    cout << "colors:" << endl;

    map<Color, uint> final_colors = pp_result.edges_of_color_in_subgraph;

    for (auto &p : final_colors) {
        if (min_color_requirements.contains(p.first))
            cout << p.first << "\t" << p.second << "\t" << (double)p.second/(double)edges.size() << " (" << min_color_requirements.at(p.first) << " req.)" << endl;
        else
            cout << p.first << "\t" << p.second << "\t" << (double)p.second/(double)edges.size() << endl;
    }

    return {density, pp_result.rt, nodes.size(), edges.size(), final_colors};
}