#include <map>
#include <set>
#include <iostream>
#include <numeric>
#include "unconstrainedDSPApprox.h"

using namespace std;

pair<uint, uint> computeUnconstrainedDSPApproximation(graph const &graph){

    std::map<uint, std::vector<edge>> g;

    NodeId mxnid = graph.max_nid;
    for (auto &e : graph.edges) {
        g[e.u].push_back(e); // todo assumption graph is undirected
        g[e.v].push_back({e.v, e.u}); // todo assumption graph is undirected
    }

    std::set<std::pair<uint, NodeId>, std::less<>> degrees;
    std::vector<uint> c(mxnid + 1, 0);
    for (auto &p : g) {
        degrees.insert({p.second.size(), p.first});
        c[p.first] = p.second.size();
    }

    std::vector<bool> removed(mxnid + 1, false);

    uint nodes = graph.max_nid + 1;
    uint edges = graph.edges.size();

    double max_density = 0;
    uint max_d_nodes = 0;
    uint max_d_edges = 0;

    while (!degrees.empty()) {

        auto next = degrees.begin();
        auto u = next->second;
        degrees.erase(next);

        if (removed[u]) continue;
        removed[u] = true;

        auto density = (double)edges/(double)nodes;
        if (density > max_density) {
            max_density = density;

            max_d_nodes = nodes;
            max_d_edges = edges;
        }

        nodes--;

        for (auto &e : g[u]) {
            if (removed[e.v]) continue;
            if (c[e.v] > c[e.u]) {
                --c[e.v];
                degrees.insert({c[e.v], e.v});

            }
            edges--;
        }

    }

    cout << "max density: " << max_density << endl;

    return {max_d_nodes, max_d_edges};
}


pair<set<NodeId>, vector<edge>> computeUnconstrainedDSPApproximationAndGetGraph(graph const &graph) {
    std::map<uint, std::vector<edge>> g;

    NodeId mxnid = graph.max_nid;
    for (auto &e : graph.edges) {
        g[e.u].push_back(e);
        g[e.v].push_back({e.v, e.u});
    }

    std::set<std::pair<uint, NodeId>, std::less<>> degrees;
    std::vector<uint> c(mxnid + 1, 0);
    for (auto &p : g) {
        degrees.insert({p.second.size(), p.first});
        c[p.first] = p.second.size();
    }

    std::vector<bool> removed(mxnid + 1, false);

    uint nodes = graph.max_nid + 1;
    uint edges = graph.edges.size();
    double max_density = 0;

    vector<NodeId> removed_nids;
    vector<NodeId> best_removed;

    auto density = (double)edges/(double)nodes;
    if (density > max_density) {
        max_density = density;
    }

    while (!degrees.empty()) {

        auto next = degrees.begin();
        auto u = next->second;
        degrees.erase(next);

        if (removed[u]) continue;
        removed[u] = true;
        removed_nids.push_back(u);

        nodes--;

        for (auto &e : g[u]) {
            if (removed[e.v]) continue;
            if (c[e.v] > c[e.u]) {
                --c[e.v];
                degrees.insert({c[e.v], e.v});

            }
            edges--;
        }

        density = (double)edges/(double)nodes;
        if (density > max_density) {
            max_density = density;
            best_removed = removed_nids;
        }

    }

    cout << "max density: " << max_density << endl;
    set<NodeId> removed_set{best_removed.begin(), best_removed.end()};
    set<NodeId> remaining_nids;
    for (int i = 0; i <= graph.max_nid; ++i) {
        if (removed_set.find(i) == removed_set.end())
            remaining_nids.insert(i);
    }

    vector<edge> remaining_edges;
    for (auto &e : graph.edges) {
        if (remaining_nids.find(e.u) != remaining_nids.end() && remaining_nids.find(e.v) != remaining_nids.end()) {
            remaining_edges.push_back(e);
        }
    }
    cout << "nodes: " << remaining_nids.size() << endl;
    cout << "edges: " << remaining_edges.size() << endl;

    return {remaining_nids, remaining_edges};
}