#ifndef FAIREDSP_ATLEASTHEDGESDSPAPPROX_H
#define FAIREDSP_ATLEASTHEDGESDSPAPPROX_H


#include "../graph/graph.h"
#include <set>

std::pair<std::set<NodeId>, std::vector<edge>> computeAtLeasthEdgesDSPApproximation(graph const &g, uint h);

double computeAtLeasthEdgesDSPApproximation_rt(graph const &graph, uint h);

#endif //FAIREDSP_ATLEASTHEDGESDSPAPPROX_H
