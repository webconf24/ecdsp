#include <map>
#include <set>
#include <iostream>
#include "atLeasthEdgesDSPApprox.h"
#include "../util/IO.h"


using namespace std;

pair<set<NodeId>, vector<edge>> computeAtLeasthEdgesDSPApproximation(graph const &graph, uint h) {
    Timer timer;
    timer.start();

    std::map<uint, std::vector<edge>> g;

    NodeId mxnid = graph.max_nid;
    for (auto &e : graph.edges) {
        g[e.u].push_back(e);
        g[e.v].push_back({e.v, e.u});
    }

    std::set<std::pair<uint, NodeId>, std::less<>> degrees;
    std::vector<uint> c(mxnid + 1, 0);
    for (auto &p : g) {
        degrees.insert({p.second.size(), p.first});
        c[p.first] = p.second.size();
    }

    std::vector<bool> removed(mxnid + 1, false);

    uint nodes = graph.max_nid + 1;
    uint edges = graph.edges.size();
    double max_density = 0;

    vector<NodeId> removed_nids;
    vector<NodeId> best_removed;

    auto density = (double)edges/(double)nodes;
    if (density > max_density) {
        max_density = density;
    }

    while (!degrees.empty()) {

        auto next = degrees.begin();
        auto u = next->second;
        degrees.erase(next);

        if (removed[u]) continue;
        removed[u] = true;
        removed_nids.push_back(u);


        nodes--;

        for (auto &e : g[u]) {
            if (removed[e.v]) continue;
            if (c[e.v] > c[e.u]) {
                --c[e.v];
                degrees.insert({c[e.v], e.v});

            }
            edges--;
        }

        if (edges < h) break;

        density = (double)edges/(double)nodes;
        if (density > max_density) {
            max_density = density;
            best_removed = removed_nids;
        }

    }
    timer.stopAndPrintTime();

    cout << "max density: " << max_density << endl;
    set<NodeId> removed_set{best_removed.begin(), best_removed.end()};
    set<NodeId> remaining_nids;
    for (NodeId i = 0; i <= graph.max_nid; ++i) {
        if (removed_set.find(i) == removed_set.end())
            remaining_nids.insert(i);
    }

    vector<edge> remaining_edges;
    for (auto &e : graph.edges) {
        if (remaining_nids.find(e.u) != remaining_nids.end() && remaining_nids.find(e.v) != remaining_nids.end()) {
            remaining_edges.push_back(e);
        }
    }
    cout << "nodes: " << remaining_nids.size() << endl;
    cout << "edges: " << remaining_edges.size() << endl;

    return {remaining_nids, remaining_edges};
}


double computeAtLeasthEdgesDSPApproximation_rt(graph const &graph, uint h) {
    Timer timer;

    std::map<uint, std::vector<edge>> g;

    NodeId mxnid = graph.max_nid;
    for (auto &e : graph.edges) {
        g[e.u].push_back(e);
        g[e.v].push_back({e.v, e.u});
    }

    timer.start();

    std::set<std::pair<uint, NodeId>, std::less<>> degrees;
    std::vector<uint> c(mxnid + 1, 0);
    for (auto &p : g) {
        degrees.insert({p.second.size(), p.first});
        c[p.first] = p.second.size();
    }

    std::vector<bool> removed(mxnid + 1, false);

    uint nodes = graph.max_nid + 1;
    uint edges = graph.edges.size();
    double max_density = 0;

    std::vector<bool> best_removed(mxnid + 1, false);

    auto density = (double)edges/(double)nodes;
    if (density > max_density) {
        max_density = density;
    }

    while (!degrees.empty()) {

        auto next = degrees.begin();
        auto u = next->second;
        degrees.erase(next);

        if (removed[u]) continue;
        removed[u] = true;

        nodes--;

        for (auto &e : g[u]) {
            if (removed[e.v]) continue;
            if (c[e.v] > c[e.u]) {
                --c[e.v];
                degrees.insert({c[e.v], e.v});

            }
            edges--;
        }

        if (edges < h) break;

        density = (double)edges/(double)nodes;
        if (density > max_density) {
            max_density = density;
            best_removed = removed;
        }

    }
    set<NodeId> remaining_nids;
    for (NodeId i = 0; i <= graph.max_nid; ++i) {
        if (!best_removed[i])
            remaining_nids.insert(i);
    }

    auto rt = timer.stopAndPrintTime();

    cout << "|S|: " << remaining_nids.size() << endl;

    return rt;
}