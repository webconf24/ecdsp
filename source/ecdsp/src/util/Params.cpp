#include <vector>
#include <iostream>
#include "Params.h"

using namespace std;

void show_help() {
    std::cout << "ecdsp dataset [-t=mode] [-x=num. nodes or edges or rounds] [-c=num. colors of edges, e.g., -c=1 100] [-m=1 for adding a single edge with a new color]" << std::endl;
    cout << "modes:\n";
    cout << ComputeAtLeastxEdgesDSP << "\tCompute at least h edges DSP" << endl;
    cout << ComputeAtLeastxEdgesDSPmultiple << "\tCompute at least h edges DSP (multiple runs) - use x=10 for plots" << endl;
    cout << ComputeAtLeastxEdgesDSPrunningtime << "\tCompute at least h edges DSP (multiple runs, for running times) - use x=10 for plots" << endl;
    cout << ComputeAtLeastxColoredEdgesDSP << "\tCompute at least h colored edges DSP" << endl;
    cout << ComputeAtLeastxColoredEdgesDSPAll_rndselection << "\tCompute at least h colored edges DSP (x random instances) - use x=100 for plots" << endl;
    cout << ComputeAtLeastxColoredEdgesDSPAll_colorfractions << "\tCompute at least h colored edges DSP (color fractions, x for number of fractions) - use x=10 for plots" << endl;
    cout << ComputeAtLeastxColoredEdgesDSPAll_runningtimes << "\tCompute at least h colored edges DSP (for running times, x for number of fractions) - use x=10 for plots" << endl;
    cout << ComputeAtLeastxColoredEdgesDSPBaseline << "\tCompute at least h colored edges DSP with baseline heuristic" << endl;
    cout << GraphInfo << "\tPrint graph info" << endl;
}

bool Params::parseArgs(vector<string> args) {
    if (args.size() < 2) {
        return false;
    }
    for (auto & arg : args) {
        cout << arg << " ";
    }
    cout << endl;

    try {
        dataset_path = args[0];
    } catch (...) {
        return false;
    }

    for (size_t i = 1; i < args.size(); ++i) {
        string next = args[i];
        if (next.length() < 4) return false;
        if (next.substr(0, 3) == "-x=") {
            string valstr = next.substr(3);
            x = stoul(valstr);
            cout << "set x " << x << endl;
        } else if (next.substr(0, 3) == "-m=") {
            string valstr = next.substr(3);
            m = stoul(valstr);
            cout << "set m " << m << endl;
        } else if (next.substr(0, 3) == "-t=") {
            string valstr = next.substr(3);
            task = static_cast<Task>(stoi(valstr));
            cout << "set task " << task << endl;
        } else if (next.substr(0, 3) == "-c=") {
            string valstr = next.substr(3);
            auto c = static_cast<Task>(stoi(valstr));
            auto r = static_cast<Task>(stoi(args[++i]));
            color_requirements[c] = r;
            cout << "set color requirement " << c << " to " << r << endl;
        } else if (next.substr(0, 3) == "-y=") {
            string valstr = next.substr(3);
            rndseed = stoi(valstr);
            srand(rndseed);
            cout << "set rndseed " << rndseed << endl;
        } else {
            return false;
        }
    }
    return true;
}